package com.zell.musicplayer.db;


public class PropertiesList {
    public static String LIBRARY_TYPE = "LibraryType";
    public static String CURRENT_SONG = "currentSong";
    public static String EQUALIZER = "Equalizer";
    public static String CURRENT_PRESET = "CurrentPreset";
    public static String DELIMITER = "_";
    public static String ECHO_CANCELER = "AcousticEchoCanceler";
    public static String NOIZE_SUPPRESSOR = "NoiseSuppressor";
    public static String BASS_BOOST = "BassBoost";
    public static String VOLUME_LEVEL = "Volume";
}
